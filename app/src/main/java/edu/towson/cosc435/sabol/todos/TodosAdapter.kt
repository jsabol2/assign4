package edu.towson.cosc435.sabol.todos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc435.sabol.todos.interfaces.TodoController
import edu.towson.cosc435.sabol.todos.model.ModelObject
import kotlinx.android.synthetic.main.todo_view.view.*


class TodosAdapter(private val controller: TodoController) : RecyclerView.Adapter<TodoViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.todo_view, parent, false)
        val viewHolder = TodoViewHolder(view)

        view.setOnLongClickListener {
            val position = viewHolder.adapterPosition
            controller.delete(position)
            this.notifyItemRemoved(position)
            return@setOnLongClickListener true
        }
        view.isCompleted.setOnClickListener {
            val position = viewHolder.adapterPosition
            controller.toggleAwesome(position)
            this.notifyItemChanged(position)
        }

        return viewHolder
    }


    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo = controller.todos.getSong(position)
        holder.bindSong(todo)
    }

    override fun getItemCount(): Int {
        return controller.todos.getCount()
    }
}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindSong(todo: ModelObject) {
        itemView.title.text = todo.title
        itemView.contents.text = todo.contents
        itemView.isCompleted.isChecked = todo.isCompleted
        itemView.date.text = todo.dateCreated
    }
}