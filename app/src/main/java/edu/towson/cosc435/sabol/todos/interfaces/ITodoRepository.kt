package edu.towson.cosc435.sabol.todos.interfaces

import edu.towson.cosc435.sabol.todos.model.ModelObject

interface ITodoRepository {
    fun getCount(): Int
    fun getSong(idx: Int): ModelObject
    fun getAll(): List<ModelObject>
    fun remove(todo: ModelObject)
    fun replace(idx: Int, todo: ModelObject)
    fun addSong(todo: ModelObject)
}