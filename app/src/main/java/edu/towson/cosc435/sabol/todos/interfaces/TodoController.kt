package edu.towson.cosc435.sabol.todos.interfaces

import edu.towson.cosc435.sabol.todos.TodoRepository

interface TodoController{
    fun delete(idx: Int)
    fun toggleAwesome(idx: Int)
    fun launchNewTodoScreen()
    val todos: ITodoRepository
}