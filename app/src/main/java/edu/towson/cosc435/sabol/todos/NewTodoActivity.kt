package edu.towson.cosc435.sabol.todos

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import edu.towson.cosc435.sabol.todos.model.ModelObject
import kotlinx.android.synthetic.main.activity_new_todo.*
import kotlinx.android.synthetic.main.todo_view.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class NewTodoActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        save.setOnClickListener{ handleAddSongClick() }

    }
    private fun handleAddSongClick() {
        val intent = Intent()

        //val trackNum = try {
        //    songTrackEt.editableText.toString().toInt()
        //} catch (e: Exception) {
        //    0
        //}

        val todo = ModelObject(
            title = inputTitle.editableText.toString(),
            contents = editText3.editableText.toString(),
            isCompleted = completed.isChecked,
            dateCreated = date.toString()
        )

        val json: String = Gson().toJson(todo)

        intent.putExtra(TODO_EXTRA_KEY, json)

        setResult(Activity.RESULT_OK, intent)

        finish()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(view: View?){
        when(view?.id){
            R.id.save -> {
                val intent = Intent()

                val title = inputTitle.editableText.toString()
                val contents = editText3.editableText.toString()
                val isCompleted = completed.isChecked
                val date = LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL))

                val todoItem = ModelObject(title,contents,isCompleted,date)
                val json = Gson().toJson(todoItem)

                intent.putExtra(TODO_EXTRA_KEY,json)
                setResult(Activity.RESULT_OK,intent)
                finish()
            }
        }
    }
    companion object{
        val TODO_EXTRA_KEY = "TODO"
    }
}
