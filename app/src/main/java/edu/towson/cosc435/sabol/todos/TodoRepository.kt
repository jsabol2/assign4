package edu.towson.cosc435.sabol.todos

import edu.towson.cosc435.sabol.todos.interfaces.ITodoRepository
import edu.towson.cosc435.sabol.todos.model.ModelObject

class TodoRepository : ITodoRepository {
    private var todos: MutableList<ModelObject> = mutableListOf()
    var flag = false

    init{
        val seed = (1..10).map { idx -> ModelObject("Todo${idx}", "Contents${idx}", flag, "test") }
        todos.addAll(seed)
    }

    override fun addSong(todo: ModelObject) {
        todos.add(todo)
    }

    override fun getCount(): Int {
        return todos.size
    }

    override fun getSong(idx: Int): ModelObject {
        return todos.get(idx)
    }

    override fun getAll(): List<ModelObject> {
        return todos
    }

    override fun remove(todo: ModelObject) {
        todos.remove(todo)
    }

    override fun replace(idx: Int, todo: ModelObject) {
        if(idx >= todos.size) throw Exception("Outside of bounds")
        todos[idx] = todo
    }
}