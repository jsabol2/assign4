package edu.towson.cosc435.sabol.todos.model

data class ModelObject(
    val title: String,
    val contents: String,
    //val image: String, // place holder
    val isCompleted: Boolean,
    val dateCreated: String
)