package edu.towson.cosc435.sabol.todos

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import edu.towson.cosc435.sabol.todos.interfaces.ITodoRepository
import edu.towson.cosc435.sabol.todos.interfaces.TodoController
import edu.towson.cosc435.sabol.todos.model.ModelObject
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), TodoController {
    private val adapter = TodosAdapter(this)

    override lateinit var todos: ITodoRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener{
            val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, ADD_SONG_REQUEST_CODE)
        }
    }

    override fun delete(idx: Int) {
        val adapter = TodosAdapter(this)
        todos.remove(todos.getSong(idx))
        adapter.notifyItemRemoved(idx)
    }

    override fun toggleAwesome(idx: Int) {
        val todo = todos.getSong(idx)
        val newSong = todo.copy(isCompleted = !todo.isCompleted)
        todos.replace(idx, newSong)
    }

    override fun launchNewTodoScreen() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, ADD_SONG_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        super.onActivityResult(requestCode,resultCode,data)

        when(resultCode){
            Activity.RESULT_OK->{
                when(requestCode){
                    ADD_SONG_REQUEST_CODE -> {
                        val json: String? = data?.getStringExtra(NewTodoActivity.TODO_EXTRA_KEY)
                        if(json != null){
                            val todo = Gson().fromJson<ModelObject>(json, ModelObject::class.java)
                            Log.v("TAG",todo.toString())
                            todos.addSong(todo)
                            adapter.notifyDataSetChanged()
                        }
                    }
                }
            }
        }

    }



    companion object {
        val ADD_SONG_REQUEST_CODE = 1
    }
}
